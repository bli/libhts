#!/bin/sh
/usr/bin/env python3 setup.py build_ext
# .egg-link does not work with PYTHONPATH ?
# Install custom pybedtools first
/usr/bin/env python3 -m pip install -r requirements.txt
/usr/bin/env python3 -m pip install -e .
/usr/bin/env python3 -m pip install --no-deps --ignore-installed .
