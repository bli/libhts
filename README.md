# Miscellaneous things to process high throughput sequencing data.

This Python package also installs some shell scripts:

* `sam2indexedbam.sh` (depends on samtools)
* `bam2bigwig.sh` (depends on bedops, python3, samtools, bedtools, niceload from parallel and bedGraphToBigWig from UCSC-tools)

It also provides some Python scripts.

* `copypaste_bigwig_regions.py` to transfer bigwig data from one region to another.
* `extract_annot_start.py` to extract starting portions of bed records.


## Installing

Get the source using `git clone git@gitlab.pasteur.fr:bli/libhts.git`, `cd` into it and run `python3 -m pip install .`

It might also work directly:

    python3 -m pip install git+ssh://git@gitlab.pasteur.fr/bli/libhts.git


## Citing

If you use this package, please cite the following paper:

> Barucci et al, 2020 (doi: [10.1038/s41556-020-0462-7](https://doi.org/10.1038/s41556-020-0462-7))
