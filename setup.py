# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup, find_packages
#from Cython.Build import cythonize

name = "libhts"

# Adapted from Biopython
__version__ = "Undefined"
for line in open("%s/__init__.py" % name):
    if (line.startswith('__version__')):
        exec(line.strip())


setup(
    name=name,
    version=__version__,
    description="Miscellaneous things to process high throughput sequencing data.",
    author="Blaise Li",
    author_email="blaise.li@normalesup.org",
    license="GNU GPLv3",
    packages=find_packages(),
    scripts=[
        "scripts/bam2bigwig.sh", "scripts/sam2indexedbam.sh",
        "scripts/bed_bam2fq.py",
        "scripts/copypaste_bigwig_regions.py",
        "scripts/extract_annot_start.py"],
    install_requires=[
        "libbamutils @ git+https://gitlab.pasteur.fr/bli/libbamutils.git",
        #"libworkflows @ git+https://gitlab+deploy-token-31:isEzpsgbNf2sJMdUDy2g@gitlab.pasteur.fr/bli/libworkflows.git@744dd79b579577cb6e131653260d7990946be3ad#egg=libworkflows-0.1",
        #"libworkflows @ git+https://gitlab+deploy-token-31:isEzpsgbNf2sJMdUDy2g@gitlab.pasteur.fr/bli/libworkflows.git#egg=libworkflows-0.1",
        #"libworkflows @ git+https://gitlab+deploy-token-31:isEzpsgbNf2sJMdUDy2g@gitlab.pasteur.fr/bli/libworkflows.git",
        "libworkflows @ git+https://gitlab.pasteur.fr/bli/libworkflows.git@b29b854ff1db6c87386007808286207b8af11b9d",
        "matplotlib",
        "networkx",
        "numpy",
        "pandas",
        "pyBigWig",
        "pybedtools @ git+https://github.com/blaiseli/pybedtools.git@issue-343",
        # "pybedtools",
        # "scipy<=1.3.3",
        "scipy>=1.4.2",
        "seaborn"],
    # specific versions and index would rather belong to requirements.txt
    #dependency_links=["git+https://gitlab.pasteur.fr/bli/libworkflows@744dd79b579577cb6e131653260d7990946be3ad#egg=libworkflows-0.1"],
    #dependency_links=["git+https://gitlab.pasteur.fr/bli/libworkflows#egg=libworkflows-0.1"]
    #dependency_links=["git+https://gitlab+deploy-token-31:isEzpsgbNf2sJMdUDy2g@gitlab.pasteur.fr/bli/libworkflows.git@744dd79b579577cb6e131653260d7990946be3ad#egg=libworkflows-0.1"]
    )
    #ext_modules = cythonize("libsmallrna/libsmallrna.pyx"),
    #install_requires=["cytoolz"],
    #zip_safe=False
