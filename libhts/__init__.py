from subprocess import check_output

__copyright__ = "Copyright (C) 2020-2022 Blaise Li"
__licence__ = "GNU GPLv3"
__version__ = 0.7

BEDTOOLS_VERSION = check_output(
    ["bedtools", "--version"]).decode("utf-8").strip().split()[-1]

from .libhts import (
    aligner2min_mapq,
    gtf_2_genes_exon_lengths,
    id_list_gtf2bed,
    make_empty_bigwig,
    make_seeding_function,
    median_ratio_to_pseudo_ref_size_factors,
    paste_bigwig_region,
    plot_boxplots, plot_counts_distribution, plot_histo,
    plot_lfc_distribution, plot_MA,
    plot_norm_correlations, plot_paired_scatters, plot_scatter,
    repeat_bed_2_lengths, size_factor_correlations,
    spikein_gtf_2_lengths, status_setter)
