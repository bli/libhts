import os
import shutil
import tempfile
import unittest
import libhts
import pyBigWig
import numpy as np


class TestPasteBigWig(unittest.TestCase):
    def setUp(self):
        self.bw_in_path = "data/klp7co_RPF.bw"
        self.from_region = ("klp7co", 0, 2459)
        self.to_region = ("III", (10808346-20), (10808346-20)+2459)
        self.tmpdir = tempfile.mkdtemp()
        self.bw_out_path = os.path.join(
            self.tmpdir, "pasted.bw")
        libhts.paste_bigwig_region(
            self.bw_in_path, self.bw_in_path,
            self.from_region, self.to_region,
            self.bw_out_path)

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def test_preserved_region_num_bins(self):
        bw_in = pyBigWig.open(self.bw_in_path)
        bw_out = pyBigWig.open(self.bw_out_path)
        bins_in = {
            (start, stop): val
            for (start, stop, val)
            in bw_in.intervals(*self.from_region)
            if (not np.isnan(val))}
            # if (not np.isnan(val)) and val > 0}
        # contains extra (start, stop, nan),
        # that's where there was no bin
        bins_out = {
            (start, stop): val
            for (start, stop, val)
            in bw_out.intervals(*self.from_region)
            if (not np.isnan(val))}
        self.assertTrue(len(bins_in) == len(bins_out))

    def test_preserved_region_bin_contents(self):
        bw_in = pyBigWig.open(self.bw_in_path)
        bw_out = pyBigWig.open(self.bw_out_path)
        bins_in = [
            (start, stop, val)
            for (start, stop, val)
            in bw_in.intervals(*self.from_region)
            if (not np.isnan(val))]
        bins_out = [
            (start, stop, val)
            for (start, stop, val)
            in bw_out.intervals(*self.from_region)
            if (not np.isnan(val))]
        for (i, (bin_in, bin_out)) in enumerate(zip(bins_in, bins_out)):
            self.assertTrue(bin_in == bin_out)

    # There are differences, maybe due to phase differences with respect to bins
    # def test_pasted_region_num_bins(self):
    #     bw_in = pyBigWig.open(self.bw_in_path)
    #     bw_out = pyBigWig.open(self.bw_out_path)
    #     bins_in = {
    #         (start, stop): val
    #         for (start, stop, val)
    #         in bw_in.intervals(*self.from_region)
    #         if (not np.isnan(val))}
    #         # if (not np.isnan(val)) and val > 0}
    #     # contains extra (start, stop, nan),
    #     # that's where there was no bin
    #     bins_out = {
    #         (start, stop): val
    #         for (start, stop, val)
    #         in bw_out.intervals(*self.to_region)
    #         if (not np.isnan(val))}
    #     self.assertTrue(len(bins_in) == len(bins_out))

    # def test_pasted_region_bin_contents(self):
    #     bw_in = pyBigWig.open(self.bw_in_path)
    #     bw_out = pyBigWig.open(self.bw_out_path)
    #     bins_in = [
    #         (start, stop, val)
    #         for (start, stop, val)
    #         in bw_in.intervals(*self.from_region)
    #         if (not np.isnan(val))]
    #     bins_out = [
    #         (start, stop, val)
    #         for (start, stop, val)
    #         in bw_out.intervals(*self.to_region)
    #         if (not np.isnan(val))]
    #     for (i, (bin_in, bin_out)) in enumerate(zip(bins_in, bins_out)):
    #         self.assertTrue(bin_in == bin_out)


if __name__ == '__main__':
    unittest.main()
