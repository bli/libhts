#!/usr/bin/env python3
# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Copy bigwig data from a region in a bigwig file into another region.

The other region can be in the same or in another bigwig file.
The result will be written in another bigwig file.
"""

from argparse import (
    ArgumentParser,
    ArgumentDefaultsHelpFormatter, RawTextHelpFormatter)
import sys
from operator import itemgetter
from yaml import safe_load as yload
from libhts import paste_bigwig_region

CONFIG_EXAMPLE = """
# bigwig file containing the region to copy
from_bw: "data/klp7co_RPF.bw"
# bigwig file containing the region to replace
to_bw: "data/klp7co_RPF.bw"
# bigwig file in which to write the result
dest_bw: "/tmp/pasted.bw"
# region from which to copy (BED-like coordinates)
from_region:
    chrom: "klp7co"
    # zero-based coordinate
    start: 0
    # 1-based coordinate
    end: 2459
# region in which to copy (BED-like coordinates)
to_region:
    chrom: "III"
    # zero-based coordinate
    start: 10808326
    # 1-based coordinate
    end: 10810785
"""


class Formatter(ArgumentDefaultsHelpFormatter, RawTextHelpFormatter):
    """
    Hybrid help formatter.
    See comments of <https://stackoverflow.com/a/3853776/1878788>
    """


def main():
    """Run the command-line script."""
    # parser = ArgumentParser(
    #     description=__doc__,
    #     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = ArgumentParser(
        description=f"{__doc__}\n\nExample configuration:\n{CONFIG_EXAMPLE}",
        formatter_class=Formatter)
    parser.add_argument(
        "-c", "--configfile",
        help="""YAML-formatted file containing information about:
    - input bigwig files
    - output bigwig file
    - region from which to copy
    - region where to paste
""")
    parser.add_argument(
        "-e", "--example_config",
        action="store_true",
        help="display an example configuration file")
    args = parser.parse_args()
    if args.example_config:
        print(CONFIG_EXAMPLE)
        return 0
    if not args.configfile:
        print("You must provide a configuration file using option -c. "
              "See option -e for an example.", file=sys.stderr)
        return 1
    with open(args.configfile) as config_fh:
        config = yload(config_fh)
    region_extractor = itemgetter("chrom", "start", "end")
    from_region = region_extractor(config["from_region"])
    to_region = region_extractor(config["to_region"])
    print(f"Copying {from_region} of {config['from_bw']}\n"
          f"into {to_region} of {config['to_bw']}")
    paste_bigwig_region(
        config["from_bw"], config["to_bw"],
        from_region, to_region,
        config["dest_bw"])
    print(f"Results written in {config['dest_bw']}")

    return 0


if __name__ == "__main__":
    sys.exit(main())
