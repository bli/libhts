#!/usr/bin/env python3
# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Extract the fastq reads from a bam file
corresponding to the annotations provided in a bed file.
"""

from argparse import (
    ArgumentParser,
    ArgumentDefaultsHelpFormatter)
import sys

from libbamutils import BamFile


def main():
    """Run the command-line script."""
    parser = ArgumentParser(
        description=__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-b", "--bedfile",
        required=True,
        help="Input bed file.")
    parser.add_argument(
        "-a", "--bamfile",
        required=True,
        help="Input bam file.")
    parser.add_argument(
        "-s", "--strand",
        default="Any",
        choices=["Any", "Same", "Opposite"],
        help="Choose hether to keep all reads or only "
        "those on one strand with respect to bed annotations.")
    args = parser.parse_args()

    bamfile = BamFile(args.bamfile)
    for fastq in bamfile.bedfile_to_fastq(args.bedfile):
        print(fastq, end="")
    return 0


if __name__ == "__main__":
    sys.exit(main())
