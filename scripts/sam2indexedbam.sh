#!/usr/bin/env bash
# Used to get mapping data suitable for making pileups or viewing with IGV
# Usage: sam2indexedbam.sh <mapping.sam> [paired]
# Add the option "paired" to only keep reads mapped in proper pair
# Works if either <mapping.sam>, <mapping.sam.gz> or <mapping.sam.bz2> exists.
# Generates two files: <mapping_sorted.bam> and <mapping_sorted.bam.bai>.

# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

tmpdir=$(mktemp -dt "${PROGNAME}.XXXXXXXXXX")

cleanup()
{
    rm -rf ${tmpdir}
}


function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    cleanup
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

samtools --version

SAM=${1}
echo "Will sort and index ${SAM}"

case ${SAM} in
    *.sam)
        ext="sam"
        ;;
    *.bam)
        ext="bam"
        ;;
    *)
        error_exit "unrecognised file extension"
        ;;
esac

# -F 4: skip reads that did not map
if [[ ${2} ]]
then
    filter="-F 4 -f 2"
else
    filter="-F 4"
fi

if [[ ${SAMTOOLS_THREADS} ]]
then
    threads_option="--threads ${SAMTOOLS_THREADS}"
else
    threads_option=""
fi

if [[ ${SAMTOOLS_MEM} ]]
then
    mem_option="-m ${SAMTOOLS_MEM}"
else
    mem_option=""
fi

if [ -e ${SAM} ]
then
    # -u: output uncompressed bam, to save time when piping to samtools sort
    #samtools view ${threads_option} -u ${filter} ${SAM} | niceload --mem 500M samtools sort ${mem_option} ${threads_option} -T ${tmpdir} -o ${SAM%.${ext}}_sorted.bam - || error_exit "samtools sort failed"
    samtools view ${threads_option} -u ${filter} ${SAM} | samtools sort ${mem_option} ${threads_option} -T ${tmpdir} -o ${SAM%.${ext}}_sorted.bam - || error_exit "samtools sort failed"
    #cat ${SAM} | samtools view ${threads_option} -u ${filter} - | samtools sort ${threads_option} -o ${SAM%.${ext}}_sorted.bam - || error_exit "samtools sort failed"
elif [ -e ${SAM}.gz ]
then
    #zcat ${SAM}.gz | samtools view ${threads_option} -u ${filter} - | niceload --mem 500M samtools sort ${mem_option} ${threads_option} -T ${tmpdir}  -o ${SAM%.${ext}}_sorted.bam - || error_exit "samtools sort failed"
    zcat ${SAM}.gz | samtools view ${threads_option} -u ${filter} - | samtools sort ${mem_option} ${threads_option} -T ${tmpdir}  -o ${SAM%.${ext}}_sorted.bam - || error_exit "samtools sort failed"
elif [ -e ${SAM}.bz2 ]
then
    #bzcat ${SAM}.bz2 | samtools view ${threads_option} -u ${filter} - | niceload --mem 500M samtools sort ${mem_option} ${threads_option} -T ${tmpdir}  -o ${SAM%.${ext}}_sorted.bam - || error_exit "samtools sort failed"
    bzcat ${SAM}.bz2 | samtools view ${threads_option} -u ${filter} - | samtools sort ${mem_option} ${threads_option} -T ${tmpdir}  -o ${SAM%.${ext}}_sorted.bam - || error_exit "samtools sort failed"
else
    echo "${SAM} not found, neither .gz or .bz2 versions."
    echo "Aborting."
    exit 1
fi
# There are random segmentation errors with samtools index
#samtools index ${SAM%.sam}_sorted.bam || error_exit "samtools index failed"
indexed=""
while [ ! ${indexed} ]
do
    samtools index ${SAM%.${ext}}_sorted.bam && indexed="OK"
    if [ ! ${indexed} ]
    then
        rm -f ${SAM%.${ext}}_sorted.bam.bai
        echo "Indexing failed. Retrying" 1>&2
    fi
done || error_exit "samtools index failed"

cleanup

exit 0
