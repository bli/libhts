#!/usr/bin/env python3
# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Extracts the first N positions from the annotations provided in a bed file.

The result is written in bed format on the standard output.
No bounds checks are performed. Some resulting bed entries might exceed
chromosome boundaries.
"""

from argparse import (
    ArgumentParser,
    ArgumentDefaultsHelpFormatter)
import sys


def make_bed_size_filter(start_size, keep_short):
    """
    Make a function that processes bed data, shortening it to *start_size*.

    Ik *keep_short* is True, the input bed data will be filtered out
    if its size is shorter than *start_size*.
    """

    def bed_size_filter(chrom, start, end, name, score, strand):
        """Print shortened bed input."""
        too_short = 0
        if int(end) - int(start) < start_size:
            if keep_short:
                sys.stderr.write(
                    "Extracted fragment will be longer "
                    f"than annotation size for {name}\n")
                too_short = 1
            else:
                return 1
        if strand == "-":
            print(
                chrom, int(end) - start_size, int(end),
                name, score, strand, sep="\t")
        else:
            print(
                chrom, int(start), int(start) + start_size,
                name, score, strand, sep="\t")
        return too_short
    return bed_size_filter


def keep_all_bed(chrom, start, end, name, score, strand):
    """Just print the input, bed-formatted."""
    print(
        chrom, int(start), int(end),
        name, score, strand, sep="\t")
    return 0


def main():
    """Run the command-line script."""
    parser = ArgumentParser(
        description=__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-b", "--bedfile",
        required=True,
        help="Input bed file.")
    parser.add_argument(
        "-g", "--gene_list",
        help="File containing a list of gene identifiers. "
        "If provided, only annotations whose 4th column "
        "matches one of these identifiers will be considered.")
    parser.add_argument(
        "-s", "--start_size",
        type=int,
        default=0,
        help="Number of positions to extract.")
    parser.add_argument(
        "-k", "--keep_short",
        help="Set this option to keep annotations that are too short.",
        action="store_true")
    args = parser.parse_args()

    start_size = args.start_size
    if start_size == 0:
        bed_processor = keep_all_bed
    else:
        bed_processor = make_bed_size_filter(start_size, args.keep_short)
    if args.gene_list:
        with open(args.gene_list) as gene_list_fh:
            gene_ids = set(line.strip() for line in gene_list_fh)
    nb_too_short = 0
    with open(args.bedfile) as bedfile:
        for line in bedfile:
            (chrom, start, end, name, score, strand) = line.strip().split("\t")
            if gene_ids and name not in gene_ids:
                continue
            nb_too_short += bed_processor(
                chrom, start, end, name, score, strand)
    if nb_too_short:
        sys.stderr.write(
            f"{nb_too_short} annotations were shorter "
            "than the extracted fragment.\n")
    return 0


if __name__ == "__main__":
    sys.exit(main())
