#!/bin/bash -l
# Usage: bam2bigwig.sh <sorted.bam> <bin_file> <library_name> <orientation> <strandedness> [<normalization>] <bigwig>
# <bin_file> should be a bed file representing 10 bp bins along the genome
# <orientation> must be "all", "fwd" or "rev"
# <strandedness> must be "F" or "R"
# It can be "U" (see http://sailfish.readthedocs.io/en/master/library_type.html),
# but anything else than "R" will be treated as "F".
# <normalization> can be either a file or a number
# If it is a file, it should contain at least two columns,
# the first one with library names and the second with size factors.
# If it is a value, it shoud be the size factor.

# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Requires:
# * bedops (for bedmap) -> module load bedops/2.4.36
# * python3 -> module load Python/3.6.0
# * samtools -> module load samtools/1.9
# * bedtools -> module load bedtools/2.25.0
# * parallel (for niceload) -> module load parallel/20170122
# * UCSC-tools (for bedGraphToBigWig) -> module load UCSC-tools/v373

load_tools ()
{
     command -v bedmap || module load bedops/2.4.36
     # command -v bedmap || module load bedops/2.4.37
     command -v python3 || module load Python/3.7.2
     # command -v python3 || module load Python/3.8.1
     command -v samtools || module load samtools/1.9
     # command -v samtools || module load samtools/1.10
     command -v bedtools || module load bedtools/2.25.0
     # command -v bedtools || module load bedtools/2.29.2
     command -v niceload || module load parallel/20170122
     # command -v niceload || module load parallel/20200222
     command -v bedGraphToBigWig || module load UCSC-tools/v373
     # command -v bedGraphToBigWig || module load UCSC-tools/v390
}

workdir=$(mktemp -d)

cleanup()
{
    rm -rf ${workdir}
}

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

error_exit ()
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    cleanup
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


load_tools || error_exit "Some tools are not available"

bam=${1}
bin_file=${2}
# Example of how to generate the bin_file from iGenome data:
#    cd /Genomes/C_elegans
#    awk '$1 == "@SQ" {print $2"\t0\t"$3}' Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/WholeGenomeFasta/genome.dict \
#        | sed 's/SN://g' | sed 's/LN://' \
#        | bedops --chop 10 - \
#        > Caenorhabditis_elegans/Ensembl/WBcel235/Sequence/genome_binned_10.bed
# A genome.dict file can be extracted from a bam file: samtools view -H <bam file>

# Ensure we use a 3 columns minimal bed for bin_file:

awk '{print $1"\t"$2"\t"$3}' ${bin_file} > "${workdir}/genome_binned.bed"
bin_file="${workdir}/genome_binned.bed"

lib_name=${3}
orient=${4}
# "F" is library type is normally stranded, "R" if it is reverse-stranded
strandedness=${5}


if [ $# = 7 ]
then
    norm=${6}
    bigwig=${7}
else
    norm=""
    bigwig=${6}
fi


bam_name=$(basename ${bam})
mapping_name=${bam_name%_sorted.bam}
mapping_dir=$(dirname ${bam})

case ${orient} in
    "all")
        orient_filter=""
        ;;
    "fwd")
        if [ "${strandedness}" = "R" ]
        then
            orient_filter="-strand -"
        else
            orient_filter="-strand +"
        fi
        ;;
    "rev")
        if [ "${strandedness}" = "R" ]
        then
            orient_filter="-strand +"
        else
            orient_filter="-strand -"
        fi
        ;;
    *)
        error_exit "unrecognised orientation ${orient}"
        ;;
esac

bigwig_dir=$(dirname ${bigwig})
echo "working in ${workdir}"
mkdir -p ${bigwig_dir}

if [ "${norm}" ]
then
    if [ -f ${norm} ]
    then
        echo "reading scaling factor for ${lib_name} in ${norm}"
        size=$(grep -w "^${lib_name}" ${norm} | cut -f2)
        if [ ! ${size} ]
        then
            error_exit "size could not be found on column 2, line ${lib_name} of ${norm}"
            #cleanup && error_exit "size could not be found on column 2, line ${lib_name} of ${norm}"
        else
            echo "found ${size} for ${lib_name}"
        fi
    else
        # Not a file
        echo "assuming ${norm} is the size factor"
        size="${norm}"
    fi
    scale=$(python3 -c "print(1.0 / ${size})")
    scaling="-scale ${scale}"
    bedgraph="${workdir}/${mapping_name}_norm_${orient}_genomecov.bg"
else
    scaling=""
    bedgraph="${workdir}/${mapping_name}_${orient}_genomecov.bg"
fi

echo "getting chromosome bounds"
genome_file="${workdir}/chrom_sizes.txt"
samtools view -H ${bam} \
    | awk '$1 == "@SQ" {print $2"\t"$3}' \
    | sed 's/SN://g' | sed 's/LN://' \
    > ${genome_file} || error_exit "making ${genome_file} failed"
    #> ${genome_file} || cleanup && error_exit "making ${genome_file} failed"

compute_coverage()
{
    cmd="niceload --noswap bedtools genomecov -bg -split ${orient_filter} ${scaling} -ibam ${bam}"
    eval ${cmd} \
        | awk '{ print $1"\t"$2"\t"$3"\tid-"NR"\t"$4; }' | sort-bed - \
        || error_exit "compute_coverage failed"
        #|| cleanup && error_exit "compute_coverage failed"
}

# TODO: find how to run this with niceload (there may be an issue with the quoting)
make_bins()
{
    cmd="bedmap --faster --echo --mean --delim \"\t\" --skip-unmapped ${bin_file} -"
    eval ${cmd} || error_exit "make_bins failed"
    #eval ${cmd} || cleanup && error_exit "make_bins failed"
}

compute_coverage | make_bins \
    > ${bedgraph} || error_exit "generating bedgraph failed"
    #> ${bedgraph} || cleanup && error_exit "generating bedgraph failed"

echo "making bigwig"
niceload --noswap bedGraphToBigWig ${bedgraph} ${genome_file} ${bigwig} || error_exit "bedGraphToBigWig failed"
#bedGraphToBigWig ${bedgraph} ${genome_file} ${bigwig} || cleanup && error_exit "bedGraphToBigWig failed"

echo "removing ${bedgraph}"
rm -f ${bedgraph}

echo "removing ${genome_file}"
rm -f ${genome_file}

cleanup
# rm -rf ${workdir}

exit 0
